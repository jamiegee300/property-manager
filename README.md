# Property Tracker

# Install dependencies
npm install

# Configure database connection
Set following environment variables:
    DB_USER
    DB_HOST
    DB_DATABASE
    DB_PASSWORD
    DB_PORT

# Serve on localhost:3000
node server

# Build
grunt build

# Run tests
grunt test
