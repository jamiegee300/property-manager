(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_properties_properties_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/properties/properties.component */ "./src/app/components/properties/properties.component.ts");
/* harmony import */ var _components_property_property_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/property/property.component */ "./src/app/components/property/property.component.ts");
/* harmony import */ var _components_create_create_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/create/create.component */ "./src/app/components/create/create.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: 'app/properties', component: _components_properties_properties_component__WEBPACK_IMPORTED_MODULE_2__["PropertiesComponent"] },
    { path: 'app/property/:id', component: _components_property_property_component__WEBPACK_IMPORTED_MODULE_3__["PropertyComponent"] },
    { path: 'app/create', component: _components_create_create_component__WEBPACK_IMPORTED_MODULE_4__["CreateComponent"] },
    { path: 'app', redirectTo: 'app/properties', pathMatch: 'full' },
    { path: '', redirectTo: 'app/properties', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid nav-container\">\r\n  <app-nav></app-nav>\r\n</div>\r\n\r\n<div class=\"container app-container\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/nav/nav.component */ "./src/app/components/nav/nav.component.ts");
/* harmony import */ var _components_properties_properties_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/properties/properties.component */ "./src/app/components/properties/properties.component.ts");
/* harmony import */ var _components_property_property_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/property/property.component */ "./src/app/components/property/property.component.ts");
/* harmony import */ var _components_create_create_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/create/create.component */ "./src/app/components/create/create.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { enableProdMode } from '@angular/core';






// enableProdMode();
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_7__["NavComponent"],
                _components_properties_properties_component__WEBPACK_IMPORTED_MODULE_8__["PropertiesComponent"],
                _components_property_property_component__WEBPACK_IMPORTED_MODULE_9__["PropertyComponent"],
                _components_create_create_component__WEBPACK_IMPORTED_MODULE_10__["CreateComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/create/create.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/create/create.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/create/create.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/create/create.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <h1>Create Property</h1>\r\n\r\n    <form>\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputOwner\" class=\"col-sm-2 col-form-label\">Owner</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.owner\" id=\"inputOwner\" name=\"owner\" type=\"text\" class=\"form-control\" placeholder=\"...owner\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAirbnbId\" class=\"col-sm-2 col-form-label\">AirBnB Id</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.airbnbId\" id=\"inputAirbnbId\" name=\"airbnbid\" type=\"text\" class=\"form-control\" placeholder=\"...airbnb id\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine1\" class=\"col-sm-2 col-form-label\">Address line 1</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line1\" id=\"inputAddressLine1\" name=\"addressLine1\" type=\"text\" class=\"form-control\" placeholder=\"...address line 1\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine2\" class=\"col-sm-2 col-form-label\">Address line 2</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line2\" id=\"inputAddressLine2\" name=\"addressLine2\" type=\"text\" class=\"form-control\" placeholder=\"...address line 2\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine3\" class=\"col-sm-2 col-form-label\">Address line 3</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line3\" id=\"inputAddressLine3\" name=\"addressLine3\" type=\"text\" class=\"form-control\" placeholder=\"...address line 3\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine4\" class=\"col-sm-2 col-form-label\">Address line 4</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line4\" id=\"inputAddressLine4\" name=\"addressLine4\" type=\"text\" class=\"form-control\" placeholder=\"...address line 4\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressPostCode\" class=\"col-sm-2 col-form-label\">Post Code</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.postCode\" id=\"inputAddressPostCode\" name=\"addressPostCode\" type=\"text\" class=\"form-control\" placeholder=\"...post code\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressCity\" class=\"col-sm-2 col-form-label\">City</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.city\" id=\"inputAddressCity\" name=\"addressCity\" type=\"text\" class=\"form-control\" placeholder=\"...city\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressCountry\" class=\"col-sm-2 col-form-label\">Country</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.country\" id=\"inputAddressCountry\" name=\"addressCountry\" type=\"text\" class=\"form-control\" placeholder=\"...country\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputNumberOfBedrooms\" class=\"col-sm-2 col-form-label\">Number of Bedrooms</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.numberOfBedrooms\" id=\"inputNumberOfBedrooms\" name=\"numberOfBedrooms\" type=\"text\" class=\"form-control\" placeholder=\"...bedrooms\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputNumberOfBathrooms\" class=\"col-sm-2 col-form-label\">Number of Bathrooms</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.numberOfBathrooms\" id=\"inputNumberOfBathrooms\" name=\"numberOfBathrooms\" type=\"text\" class=\"form-control\" placeholder=\"...bathrooms\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputIncomeGenerated\" class=\"col-sm-2 col-form-label\">Income Generated</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.incomeGenerated\" id=\"inputIncomeGenerated\" name=\"incomeGenerated\" type=\"text\" class=\"form-control\" placeholder=\"...income generated\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <div class=\"col-sm-6\">\r\n          <button class=\"btn btn-primary\" (click)=\"create()\">Submit</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <div class=\"alert alert-success\" role=\"alert\" *ngIf=\"feedback && feedback.length\">\r\n      <p>{{feedback}}</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/create/create.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/create/create.component.ts ***!
  \*******************************************************/
/*! exports provided: CreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateComponent", function() { return CreateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_http_resource_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/http-resource.service */ "./src/app/services/http-resource.service.ts");
/* harmony import */ var _model_Property__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/Property */ "./src/app/model/Property.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CreateComponent = /** @class */ (function () {
    function CreateComponent(httpResourceService) {
        this.httpResourceService = httpResourceService;
        this.createUrl = "/api/property/create";
        this.airBnbIdUrl = "/api/airbandbid";
    }
    CreateComponent.prototype.ngOnInit = function () {
        this.property = new _model_Property__WEBPACK_IMPORTED_MODULE_2__["Property"]();
    };
    CreateComponent.prototype.sendProperty = function () {
        this.httpResourceService.send(this.createUrl, this.property)
            .subscribe(function (result) {
        });
    };
    CreateComponent.prototype.create = function () {
        var _this = this;
        var url = this.airBnbIdUrl + '/' + this.property.airbnbId;
        this.httpResourceService.retrieve(url, this.property)
            .subscribe(function (result) {
            console.log("RESULT: ", result);
            //if (result.status === 200)
            //this.sendProperty();
            _this.feedback = "Invalid Air BnB Id";
        });
    };
    CreateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create',
            template: __webpack_require__(/*! ./create.component.html */ "./src/app/components/create/create.component.html"),
            styles: [__webpack_require__(/*! ./create.component.css */ "./src/app/components/create/create.component.css")]
        }),
        __metadata("design:paramtypes", [_services_http_resource_service__WEBPACK_IMPORTED_MODULE_1__["HttpResourceService"]])
    ], CreateComponent);
    return CreateComponent;
}());



/***/ }),

/***/ "./src/app/components/nav/nav.component.css":
/*!**************************************************!*\
  !*** ./src/app/components/nav/nav.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:hover {\r\n  cursor: pointer;\r\n}\r\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/nav/nav.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <a class=\"navbar-brand\" href=\"#\">Property Manager</a>\r\n\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n\r\n      <li class=\"nav-item\" [ngClass]=\"setActiveClass('/app/properties')\">\r\n        <a class=\"nav-link\" (click)=\"goTo('/app/properties')\">Properties</a>\r\n      </li>\r\n\r\n      <li class=\"nav-item\" [ngClass]=\"setActiveClass('/app/create')\">\r\n        <a class=\"nav-link\" (click)=\"goTo('/app/create')\">Create</a>\r\n      </li>\r\n\r\n    </ul>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/nav/nav.component.ts ***!
  \*************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavComponent = /** @class */ (function () {
    function NavComponent(router) {
        var _this = this;
        this.router = router;
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                console.log('Started change route');
            }
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                console.log('Finished change route', event);
                _this.currentPath = event.url;
            }
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationError"]) {
                console.log('Error changing route', event.error);
            }
        });
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent.prototype.setActiveClass = function (path) {
        return { 'active': path === this.currentPath };
    };
    NavComponent.prototype.goTo = function (path) {
        this.router.navigateByUrl(path);
    };
    NavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! ./nav.component.html */ "./src/app/components/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/components/nav/nav.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/components/properties/properties.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/properties/properties.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".list-group-item-action {\r\n  cursor: pointer;\r\n}\r\n"

/***/ }),

/***/ "./src/app/components/properties/properties.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/properties/properties.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <h1>Properties</h1>\r\n\r\n    <div class=\"form-group\">\r\n      <label for=\"bedroomSelect\" class=\"col-sm-2 col-form-label\">Bedrooms</label>\r\n\r\n      <select id=\"bedroomSelect\" class=\"form-control col-md-6\" (change)=\"search()\" [(ngModel)]=\"selectedBedrooms\">\r\n        <option *ngFor=\"let option of bedroomOptions\" [value]=\"option.value\">{{option.text}}</option>\r\n      </select>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label for=\"bathroomSelect\" class=\"col-sm-2 col-form-label\">Bathrooms</label>\r\n\r\n      <select id=\"bathroomSelect\" class=\"form-control col-md-6\" (change)=\"search()\" [(ngModel)]=\"selectedBathrooms\">\r\n        <option *ngFor=\"let option of bathroomOptions\" [value]=\"option.value\">{{option.text}}</option>\r\n      </select>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n      <div class=\"list-group col-md-6\" *ngIf=\"properties && properties.length\">\r\n        <a *ngFor=\"let property of properties\" (click)=\"goToProperty(property.id)\" class=\"list-group-item list-group-item-action\">\r\n          {{property.numberOfBedrooms}} bedrooms in {{property.address.city}} ({{property.address.line1}})\r\n        </a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/properties/properties.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/properties/properties.component.ts ***!
  \***************************************************************/
/*! exports provided: PropertiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropertiesComponent", function() { return PropertiesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_http_resource_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/http-resource.service */ "./src/app/services/http-resource.service.ts");
/* harmony import */ var _model_Property__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/Property */ "./src/app/model/Property.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PropertiesComponent = /** @class */ (function () {
    function PropertiesComponent(router, httpResourceService) {
        this.router = router;
        this.httpResourceService = httpResourceService;
        this.propertiesUrl = "/api/properties";
        this.selectedBedrooms = "any";
        this.selectedBathrooms = "any";
        this.bedroomOptions = [
            { text: "Any", value: "any" },
            { text: "2", value: "2" },
            { text: "3", value: "3" },
            { text: "4", value: "4" },
            { text: "5+", value: "5+" },
        ];
        this.bathroomOptions = [
            { text: "Any", value: "any" },
            { text: "2", value: "2" },
            { text: "3", value: "3" },
            { text: "4", value: "4" },
            { text: "5+", value: "5+" },
        ];
    }
    PropertiesComponent.prototype.ngOnInit = function () {
        this.search();
    };
    PropertiesComponent.prototype.search = function () {
        var _this = this;
        var uri = this.propertiesUrl +
            "?bedrooms=" + this.selectedBedrooms +
            "&bathrooms=" + this.selectedBathrooms;
        this.httpResourceService.retrieve(uri)
            .subscribe(function (results) {
            console.log("RESULTS: ", results);
            _this.properties = results['properties'].map(function (value) {
                return new _model_Property__WEBPACK_IMPORTED_MODULE_3__["Property"](value);
            });
        });
    };
    PropertiesComponent.prototype.goToProperty = function (id) {
        this.router.navigateByUrl('/app/property/' + id);
    };
    PropertiesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-properties',
            template: __webpack_require__(/*! ./properties.component.html */ "./src/app/components/properties/properties.component.html"),
            styles: [__webpack_require__(/*! ./properties.component.css */ "./src/app/components/properties/properties.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_http_resource_service__WEBPACK_IMPORTED_MODULE_2__["HttpResourceService"]])
    ], PropertiesComponent);
    return PropertiesComponent;
}());



/***/ }),

/***/ "./src/app/components/property/property.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/property/property.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/property/property.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/property/property.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <h1>Property Details</h1>\r\n\r\n    <form *ngIf=\"property\">\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputOwner\" class=\"col-sm-2 col-form-label\">Owner</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.owner\" id=\"inputOwner\" name=\"owner\" type=\"text\" class=\"form-control\" placeholder=\"...owner\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAirbnbId\" class=\"col-sm-2 col-form-label\">AirBnB Id</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.airbnbId\" id=\"inputAirbnbId\" name=\"airbnbid\" type=\"text\" class=\"form-control\" placeholder=\"...airbnb id\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine1\" class=\"col-sm-2 col-form-label\">Address line 1</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line1\" id=\"inputAddressLine1\" name=\"addressLine1\" type=\"text\" class=\"form-control\" placeholder=\"...address line 1\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine2\" class=\"col-sm-2 col-form-label\">Address line 2</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line2\" id=\"inputAddressLine2\" name=\"addressLine2\" type=\"text\" class=\"form-control\" placeholder=\"...address line 2\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine3\" class=\"col-sm-2 col-form-label\">Address line 3</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line3\" id=\"inputAddressLine3\" name=\"addressLine3\" type=\"text\" class=\"form-control\" placeholder=\"...address line 3\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressLine4\" class=\"col-sm-2 col-form-label\">Address line 4</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.line4\" id=\"inputAddressLine4\" name=\"addressLine4\" type=\"text\" class=\"form-control\" placeholder=\"...address line 4\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressPostCode\" class=\"col-sm-2 col-form-label\">Post Code</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.postCode\" id=\"inputAddressPostCode\" name=\"addressPostCode\" type=\"text\" class=\"form-control\" placeholder=\"...post code\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressCity\" class=\"col-sm-2 col-form-label\">City</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.city\" id=\"inputAddressCity\" name=\"addressCity\" type=\"text\" class=\"form-control\" placeholder=\"...city\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputAddressCountry\" class=\"col-sm-2 col-form-label\">Country</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.address.country\" id=\"inputAddressCountry\" name=\"addressCountry\" type=\"text\" class=\"form-control\" placeholder=\"...country\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputNumberOfBedrooms\" class=\"col-sm-2 col-form-label\">Number of Bedrooms</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.numberOfBedrooms\" id=\"inputNumberOfBedrooms\" name=\"numberOfBedrooms\" type=\"text\" class=\"form-control\" placeholder=\"...bedrooms\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputNumberOfBathrooms\" class=\"col-sm-2 col-form-label\">Number of Bathrooms</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.numberOfBathrooms\" id=\"inputNumberOfBathrooms\" name=\"numberOfBathrooms\" type=\"text\" class=\"form-control\" placeholder=\"...bathrooms\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"inputIncomeGenerated\" class=\"col-sm-2 col-form-label\">Income Generated</label>\r\n        <div class=\"col-sm-6\">\r\n          <input [(ngModel)]=\"property.incomeGenerated\" id=\"inputIncomeGenerated\" name=\"incomeGenerated\" type=\"text\" class=\"form-control\" placeholder=\"...income generated\" required>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <div class=\"col-sm-6\">\r\n          <button class=\"btn btn-primary\" (click)=\"update()\">Update</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <div class=\"alert alert-success\" role=\"alert\" *ngIf=\"feedback && feedback.length\">\r\n      <p>{{feedback}}</p>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/property/property.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/property/property.component.ts ***!
  \***********************************************************/
/*! exports provided: PropertyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropertyComponent", function() { return PropertyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_http_resource_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/http-resource.service */ "./src/app/services/http-resource.service.ts");
/* harmony import */ var _model_Property__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/Property */ "./src/app/model/Property.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PropertyComponent = /** @class */ (function () {
    function PropertyComponent(route, httpResourceService) {
        this.route = route;
        this.httpResourceService = httpResourceService;
        this.propertyUrl = "/api/property";
    }
    PropertyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            var uri = _this.propertyUrl + '/' + _this.id;
            _this.httpResourceService.retrieve(uri)
                .subscribe(function (result) {
                _this.property = new _model_Property__WEBPACK_IMPORTED_MODULE_3__["Property"](result['property']);
            });
        });
    };
    PropertyComponent.prototype.update = function () {
        var uri = this.propertyUrl + '/' + this.id;
        this.httpResourceService.send(uri, this.property)
            .subscribe(function (result) {
            console.log("Updated");
        });
    };
    PropertyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-property',
            template: __webpack_require__(/*! ./property.component.html */ "./src/app/components/property/property.component.html"),
            styles: [__webpack_require__(/*! ./property.component.css */ "./src/app/components/property/property.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_http_resource_service__WEBPACK_IMPORTED_MODULE_2__["HttpResourceService"]])
    ], PropertyComponent);
    return PropertyComponent;
}());



/***/ }),

/***/ "./src/app/model/Property.ts":
/*!***********************************!*\
  !*** ./src/app/model/Property.ts ***!
  \***********************************/
/*! exports provided: Property */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Property", function() { return Property; });
var Property = /** @class */ (function () {
    function Property(properties) {
        this.id = properties && properties.id || 0;
        this.airbnbId = properties && properties.airbnbId || 0;
        this.owner = properties && properties.owner || "owner";
        this.address = properties && properties.address;
        this.numberOfBedrooms = properties && properties.numberOfBedrooms || 0;
        this.numberOfBathrooms = properties && properties.numberOfBathrooms || 0;
        this.incomeGenerated = properties && properties.incomeGenerated || 0;
    }
    return Property;
}());



/***/ }),

/***/ "./src/app/services/http-resource.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/http-resource.service.ts ***!
  \***************************************************/
/*! exports provided: HttpResourceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpResourceService", function() { return HttpResourceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HttpResourceService = /** @class */ (function () {
    function HttpResourceService(http) {
        //this.csrfToken = <HTMLInputElement>document.getElementById('csrf');
        this.http = http;
        this.options = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json',
            }),
            responseType: 'json',
            observe: 'body'
        };
    }
    HttpResourceService_1 = HttpResourceService;
    HttpResourceService.appendTimestamp = function (url) {
        return url +
            ((url.indexOf('?') === -1) ? "?" : "&") +
            "now=" + new Date().valueOf();
    };
    HttpResourceService.prototype.send = function (url, data, options) {
        if (options === void 0) { options = this.options; }
        return this.http.post(url, data, options);
    };
    HttpResourceService.prototype.retrieve = function (url, options) {
        if (options === void 0) { options = this.options; }
        if (!options.allowCache)
            url = HttpResourceService_1.appendTimestamp(url);
        return this.http.get(url, options);
    };
    HttpResourceService = HttpResourceService_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HttpResourceService);
    return HttpResourceService;
    var HttpResourceService_1;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Personal\projects\property-manager\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map