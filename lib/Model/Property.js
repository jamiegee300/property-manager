"use strict";
const Address = require('./Address');

class Property {

    constructor(properties = {}) {
        if (properties.id) this.id = properties.id;
        this.airbnbId = properties.airbnbId;
        this.owner = properties.owner;
        this.address = new Address(properties.address);
        this.numberOfBedrooms = properties.numberOfBedrooms;
        this.numberOfBathrooms = properties.numberOfBathrooms;
        this.incomeGenerated = properties.incomeGenerated;
    }

    static validateFields(obj = {}) {
        const fields = {};
        if (obj.id) fields.id = obj.id;
        if (obj.owner) fields.owner = obj.owner;
        if (obj.address) fields.address = new Address(obj.address);
        if (obj.numberOfBedrooms) fields.numberOfBedrooms = obj.numberOfBedrooms;
        if (obj.numberOfBathrooms) fields.numberOfBathrooms = obj.numberOfBathrooms;
        if (obj.incomeGenerated) fields.incomeGenerated = obj.incomeGenerated;
        return fields;
    }

}

module.exports = Property;