module.exports = {
    Event: require('./Event'),
    Address: require('./Address'),
    Property: require('./Property')
};