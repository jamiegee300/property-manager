"use strict";

class Address {

    constructor(properties = {}) {
        if (properties.id) this.id = properties.id;
        if (properties.line1) this.line1 = properties.line1;
        if (properties.line2) this.line2 = properties.line2;
        if (properties.line3) this.line3 = properties.line3;
        if (properties.line4) this.line4 = properties.line4;
        if (properties.postCode) this.postCode = properties.postCode;
        if (properties.city) this.city = properties.city;
        if (properties.country) this.country = properties.country;
    }

    static validateFields(obj = {}) {
        const fields = {};
        if (obj.line1) fields.line1 = obj.line1;
        if (obj.line2) fields.line2 = obj.line2;
        if (obj.line3) fields.line3 = obj.line3;
        if (obj.line4) fields.line4 = obj.line4;
        if (obj.postCode) fields.postCode = obj.postCode;
        if (obj.city) fields.city = obj.city;
        if (obj.country) fields.country = obj.country;
        return fields;
    }

}

module.exports = Address;