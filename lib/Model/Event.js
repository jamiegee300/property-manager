"use strict";

class Event {

    constructor(properties = {}) {
        this.name = properties.name;
        this.payload = Event.serialise(properties.payload);
    }

    static reviver(key, value) {
        const match = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);

        if (typeof value === 'string' && match) {
            return new Date(Date.UTC(+match[1], +match[2] - 1, +match[3], +match[4], +match[5], +match[6]));
        }

        return value;
    }

    static serialise(obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    static deserialise(value) {
        value = value || this.data;
        try {
            value = decodeURIComponent(value);
            return JSON.parse(value, Event.reviver);
        } catch(ex) {
            throw new Error('Invalid JSON ' + ex);
        }
    }

    getPayload() {
        return Event.deserialise(this.payload);
    }
}

module.exports = Event;