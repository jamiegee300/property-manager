"use strict";
const debug = require('debug')('app:command:createEvent');

class CreateEventCommand {

    constructor(dependencies) {
        this.database = dependencies.database;
    }

    execute(parameters, done) {
        const queryToExecute = 'INSERT INTO event ( name, payload ) ' +
                               'VALUES ( "' + parameters.event + '", "' + parameters.serialisedPayload + '" )';

        debug("Executing ", queryToExecute);
        this.database.query(queryToExecute, (err) => {
            if (err) return done(err);

            done();
        });
    }

}

module.exports = CreateEventCommand;