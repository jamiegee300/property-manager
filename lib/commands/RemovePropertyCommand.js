"use strict";
const debug = require('debug')('app:command:removeProperty');

class RemovePropertyCommand {

    constructor(dependencies) {
        this.database = dependencies.database;
    }

    getAddressId(parameters) {
        return new Promise((resolve, reject) => {
            const queryToExecute = 'SELECT address FROM property ' +
                                   'WHERE id = ' + parameters.id;

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result.rows[0].address);
            });
        });
    }

    removeAddress(addressId) {
        return new Promise((resolve, reject) => {
            const queryToExecute = 'DELETE FROM address ' +
                                   'WHERE id = ' + addressId;

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    removeProperty(parameters) {
        return new Promise((resolve, reject) => {
            const queryToExecute = 'DELETE FROM property ' +
                                   'WHERE id = ' + parameters.id;

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    execute(parameters, done) {
        this.getAddressId(parameters)
            .then((addressId) => { return this.removeAddress(addressId) })
            .then(() => { return this.removeProperty(parameters); })
            .then(() => { done(); })
            .catch(err => { done(err); });
    }

}

module.exports = RemovePropertyCommand;