"use strict";
const debug = require('debug')('app:command:createProperty');

class CreatePropertyCommand {

    constructor(dependencies) {
        this.database = dependencies.database;
        this.Commands = dependencies.Commands;
    }

    createAddress(parameters) {
        return new Promise((resolve, reject) => {
            delete parameters.values.address.id;
            const values = this.Commands.createInsertStatementFromObject(parameters.values.address);
            const fields = this.Commands.getFieldNamesFromValues(parameters.values.address);

            const queryToExecute = 'INSERT INTO address ( ' + fields + ' ) ' +
                                   'VALUES ( ' + values + ' )';

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result.rows[0].id);
            });
        });
    }

    createProperty(parameters) {
        return new Promise((resolve, reject) => {
            delete parameters.values.id;
            const create = this.Commands.createInsertStatementFromObject(parameters.values);
            const fields = this.Commands.getFieldNamesFromValues(parameters.values);

            const queryToExecute = 'INSERT INTO property (' + fields + ') ' +
                                   'VALUES (' + create + ')';

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });

    }

    execute(parameters, done) {
        this.createAddress(parameters)
            .then((addressId) => {
                parameters.values.address = addressId;
                return this.createProperty(parameters);
            })
            .then(() => { done(); })
            .catch(err => { done(err); });
    }

}

module.exports = CreatePropertyCommand;