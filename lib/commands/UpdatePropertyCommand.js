"use strict";
const debug = require('debug')('app:command:updateProperty');

class UpdatePropertyCommand {

    constructor(dependencies) {
        this.database = dependencies.database;
        this.Commands = dependencies.Commands;
    }

    updateAddress(parameters) {
        return new Promise((resolve, reject) => {
            const addressId = parameters.updates.address.id;
            const update = this.Commands.createUpdateStatementFromObject(parameters.updates.address);
            delete update.id;

            const queryToExecute = 'UPDATE address ' +
                                   'SET ' + update + ' ' +
                                   'WHERE id = ' + addressId;

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    updateProperty(parameters) {
        return new Promise((resolve, reject) => {
            delete parameters.updates.address;
            const update = this.Commands.createUpdateStatementFromObject(parameters.updates);

            const queryToExecute = 'UPDATE property ' +
                                   'SET ' + update + ' ' +
                                   'WHERE id = ' + parameters.id;

            debug("Executing ", queryToExecute);
            this.database.query(queryToExecute, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    execute(parameters, done) {
        this.updateAddress(Object.assign({}, parameters))
            .then(() => { return this.updateProperty(Object.assign({}, parameters)); })
            .then(() => { done(); })
            .catch(err => { done(err); });
    }

}

module.exports = UpdatePropertyCommand;