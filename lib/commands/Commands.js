"use strict";
const _ = require('lodash');

class Command {

    constructor(dependencies) {
        this.dependencies = dependencies;
        dependencies.database.ensureSchema(dependencies.settings);
    }

    static createUpdateStatementFromObject(obj = {}) {
        const mappedValues = _(obj).map((value, key) => {
            return key + ' = "' + value + '"';
        }).value();

        return mappedValues.join(', ');
    }

    static createInsertStatementFromObject(obj = {}) {
        const mappedValues = _(obj).map((value, key) => {
            return '"' + value + '"';
        }).value();

        return mappedValues.join(', ');
    }

    static getFieldNamesFromValues(obj = {}) {
        const mappedValues = _(obj).map((value, key) => {
            return key;
        }).value();

        return mappedValues.join(', ');
    }

    createProperty(parameters, done) {
        const Cmd = require('./CreatePropertyCommand');
        const command = new Cmd(this.dependencies);
        return command.execute(parameters, done);
    }

    updateProperty(parameters, done) {
        const Cmd = require('./UpdatePropertyCommand');
        const command = new Cmd(this.dependencies);
        return command.execute(parameters, done);
    }

    removeProperty(parameters, done) {
        const Cmd = require('./RemovePropertyCommand');
        const command = new Cmd(this.dependencies);
        return command.execute(parameters, done);
    }

    createEvent(parameters, done) {
        const Cmd = require('./CreateEventCommand');
        const command = new Cmd(this.dependencies);
        return command.execute(parameters, done);
    }

}

module.exports = Command;