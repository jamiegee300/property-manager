"use strict";
const debug = require('debug')('app:database');
const { Client } = require('pg');

class Database {

    constructor(settings, cl) {
        this.settings = settings.database;
        this.client = (!cl) ? new Client(this.settings) : cl;
        this.client.connect(err => {
            if (err) debug("Error connecting to database ", err);

            this.ensureSchema()
                .then(() => { debug('Schema configured'); })
                .catch(err => { debug('Error configuring schema ', err); });
        });
    }

    ensureSchema() {
        if (!this.settings.schema || !this.settings.schema.length) {
            debug('No schema rules to apply');
            return;
        }

        const schemaRules = this.settings.schema.map(rule => {
            return new Promise((resolve, reject) => {
                this.client.query(rule, [], err => {
                    if (err) return reject(err);

                    resolve();
                });
            });
        });

        return Promise.all(schemaRules);
    }

    query(queryToExecute, done) {
        this.client.query(queryToExecute, [], (err, result) => {
            if (err) return done(err);

            done(null, result);
        });
    }
}

module.exports = Database;