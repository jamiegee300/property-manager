"use strict";
const debug = require('debug')('app:service:eventTracker');

class EventTracker {

    constructor(dependencies) {
        this.Model = dependencies.Model;
        this.commands = dependencies.commands;
    }

    track(event, payload, done) {
        done = done || function () {};
        const serialisedPayload = this.Model.Event.serialise(payload);

        this.commands.createEvent({ event, serialisedPayload }, err => {
            if (err) return done(err);

            done();
        });
    }

}

module.exports = EventTracker;