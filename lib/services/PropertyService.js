"use strict";
const debug = require('debug')('app:service:property');

class PropertyService {

    constructor(dependencies) {
        this.Model = dependencies.Model;
        this.queries = dependencies.queries;
        this.commands = dependencies.commands;
        this.eventTracker = dependencies.eventTracker;
    }

    search(parameters) {
        return new Promise((resolve, reject) => {
            const bedrooms = parameters.bedrooms;
            const bathrooms = parameters.bathrooms;

            this.queries.getPropertiesBySearchTerm({ bedrooms, bathrooms }, (err, results) => {
                if (err) return reject(err);

                resolve(results.map(res => {
                    return new this.Model.Property(res);
                }));
            });
        });
    }

    get(parameters) {
        return new Promise((resolve, reject) => {
            const id = parameters.id;

            this.queries.getPropertyById({ id }, (err, result) => {
                if (err) return reject(err);

                resolve(new this.Model.Property(result));
            });
        });
    }

    create(parameters) {
        return new Promise((resolve, reject) => {
            const values = this.Model.Property.validateFields(parameters.values);

            this.eventTracker.track('property:create', parameters);

            this.commands.createProperty({ values }, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    update(parameters) {
        return new Promise((resolve, reject) => {
            const id = parameters.id;
            const updates = this.Model.Property.validateFields(parameters.updates);

            this.eventTracker.track('property:update', parameters);

            this.commands.updateProperty({ id, updates }, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

    remove(parameters) {
        return new Promise((resolve, reject) => {
            const id = parameters.id;

            this.eventTracker.track('property:remove', parameters);

            this.commands.removeProperty({ id }, (err, result) => {
                if (err) return reject(err);

                resolve(result);
            });
        });
    }

}

module.exports = PropertyService;