"use strict";
const debug = require('debug')('app:service:airBAndB');
const request = require('superagent');

class AirBAndBService {

    constructor(dependencies) {
        this.request = dependencies.request || request;
        this.settings = dependencies.settings;
    }

    validateId(value, done) {
        const uri = this.settings.airBAndBUri + value;

        return new Promise((resolve, reject) => {
            this.request.get(uri)
                        .then(res => {
                            if (res.status === 200)
                                return resolve();

                            reject();
                        });
        });

    }

}

module.exports = AirBAndBService;