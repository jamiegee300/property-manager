'use strict';
const debug = require('debug')('app:query:getPropertiesBySearchTerm');

class GetPropertiesBySearchTermQuery {

    constructor(dependencies) {
        this.database = dependencies.database;
    }

    getWhereClause(parameters) {
        const filterByBedrooms = parameters.bedrooms && parameters.bedrooms !== 'any';
        const filterByBathrooms = parameters.bathrooms && parameters.bathrooms !== 'any';
        if (!filterByBedrooms && !filterByBathrooms) return '';
        
        let whereClause = 'WHERE ';

        if (filterByBedrooms) {
            if (parameters.bedrooms === '5+') whereClause += 'property.numberOfBedrooms >= 5';
            else whereClause += 'property.numberOfBedrooms = ' + parseInt(parameters.bedrooms, 10);
        }

        if (filterByBathrooms) {
            if (filterByBedrooms) whereClause += ' AND ';

            if (parameters.bathrooms === '5+') whereClause += 'property.numberOfBathrooms >= 5';
            else whereClause += 'property.numberOfBathrooms = ' + parseInt(parameters.bathrooms, 10);
        }

        return whereClause;
    }

    execute(parameters, done) {
        const queryToExecute = 'SELECT * ' +
                               'FROM property ' +
                               'LEFT JOIN address ' +
                               'ON property.address = address.id ' +
                               this.getWhereClause(parameters);

        debug('Executing ', queryToExecute);
        this.database.query(queryToExecute, (err, result) => {
            if (err) return done(err);

            done(null, result.rows);
        });
    }

}

module.exports = GetPropertiesBySearchTermQuery;