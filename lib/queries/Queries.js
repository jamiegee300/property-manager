"use strict";
const debug = require('debug')('tracker:queries');

class Query {

    constructor(dependencies) {
        this.dependencies = dependencies;
        dependencies.database.ensureSchema(dependencies.settings.schema);
    }

    getPropertiesBySearchTerm (parameters, done) {
        const Qry = require('./GetPropertiesBySearchTermQuery');
        const query = new Qry(this.dependencies);
        return query.execute(parameters, done);
    }

    getPropertyById (parameters, done) {
        const Qry = require('./GetPropertyByIdQuery');
        const query = new Qry(this.dependencies);
        return query.execute(parameters, done);
    }
    
}

module.exports = Query;