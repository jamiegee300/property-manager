"use strict";
const debug = require('debug')('app:query:getPropertyById');

class GetPropertyByIdQuery {

    constructor(dependencies) {
        this.database = dependencies.database;
        this.guards = dependencies.guards;
    }

    execute(parameters, done) {
        const queryToExecute = 'SELECT * ' +
                               'FROM property ' +
                               'LEFT JOIN address ' +
                               'ON property.address = address.id ' +
                               'WHERE property.id = ' + parameters.id + ' ' +
                               'LIMIT 1';

        debug("Executing ", queryToExecute);
        this.database.query(queryToExecute, (err, result) => {
            if (err) return done(err);

            done(null, result.rows[0]);
        });
    }

}

module.exports = GetPropertyByIdQuery;