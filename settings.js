module.exports = {
    database: {
        user: process.env.DB_USER || 'postgres',
        host: process.env.DB_HOST || 'localhost',
        database: process.env.DB_DATABASE || 'property_db',
        password: process.env.DB_PASSWORD || '',
        port: process.env.DB_PORT || 5432,
        schema: [
            'DROP TABLE IF EXISTS property',
            'DROP TABLE IF EXISTS address',
            'DROP TABLE IF EXISTS event',

            'CREATE TABLE IF NOT EXISTS property (id integer, airbnbId integer, owner varchar, address integer, numberOfBedrooms integer, numberOfBathrooms integer, incomeGenerated varchar)',
            'CREATE TABLE IF NOT EXISTS address (id integer, line1 varchar, line2 varchar, line3 varchar, line4 varchar, postCode varchar, city varchar, country varchar)',
            'CREATE TABLE IF NOT EXISTS event (id integer, name varchar, payload varchar)',

            'INSERT INTO address (id, line1, line4, postCode, city, country) VALUES (1, \'Flat 5\', \'7 Westbourne Terrace\', \'W2 3UL\', \'London\', \'U.K.\')',
            'INSERT INTO property (id, airbnbId, owner, address, numberOfBedrooms, numberOfBathrooms, incomeGenerated) VALUES (1, 3512500, \'carlos\', 1, 1, 1, \'2000.34\')',

            'INSERT INTO address (id, line1, line2, line3, line4, postCode, city, country) VALUES (2, \'4\', \'Tower Mansions\', \'Off Station road\', \'86-87 Grange Road\', \'SE1 3BW\', \'London\', \'U.K.\')',
            'INSERT INTO property (id, airbnbId, owner, address, numberOfBedrooms, numberOfBathrooms, incomeGenerated) VALUES (2, 1334159, \'ankur\', 2, 3, 1, \'10000\')',

            'INSERT INTO address (id, line1, line2, line4, postCode, city, country) VALUES (3, \'4\', \'332b\', \'Goswell Road\', \'EC1V 7LQ\', \'London\', \'U.K.\')',
            'INSERT INTO property (id, airbnbId, owner, address, numberOfBedrooms, numberOfBathrooms, incomeGenerated) VALUES (3, 12220057, \'elaine\', 3, 2, 2, \'1200\')'
        ]
    }
};