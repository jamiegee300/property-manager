const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const lessMiddleware = require('less-middleware');
const logger = require('morgan');
const settings = require('./settings');

const Model = require('./lib/Model');
const Commands = require('./lib/commands/Commands');
const Queries = require('./lib/queries/Queries');
const EventTracker = require('./lib/services/EventTracker');
const AirBAndBService = require('./lib/services/AirBAndBService');
const PropertyService = require('./lib/services/PropertyService');

const dependencies = { settings, Model };
const Database = require('./lib/Database');
dependencies.database = new Database(settings);
dependencies.Queries = Queries;
dependencies.queries = new Queries(dependencies);
dependencies.Commands = Commands;
dependencies.commands = new Commands(dependencies);
dependencies.eventTracker = new EventTracker(dependencies);
dependencies.airBAndBService = new AirBAndBService(dependencies);
dependencies.propertyService = new PropertyService(dependencies);

const apiRouter = require('./routes/api')(dependencies);
const indexRouter = require('./routes/index')(dependencies);
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
