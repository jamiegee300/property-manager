export class Option {
  text: string;
  value: string;

  constructor(properties?: any) {
    this.text = properties && properties.text || "";
    this.value = properties && properties.value || "";
  }
}
