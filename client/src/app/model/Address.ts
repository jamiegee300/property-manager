export class Address {
  id: number;
  line1: string;
  line2: string;
  line3: string;
  line4: string;
  postCode: string;
  city: string;
  country: string;

  constructor(properties?: any) {
    this.id = properties && properties.id || 0;
    this.line1 = properties && properties.line1 || "";
    this.line2 = properties && properties.line1 || "";
    this.line3 = properties && properties.line1 || "";
    this.line4 = properties && properties.line1 || "";
    this.postCode = properties && properties.postCode || "";
    this.city = properties && properties.city || "";
    this.country = properties && properties.country || "";
  }
}
