import { Address } from "./Address";

export class Property {
  id: number;
  airbnbId: number;
  owner: string;
  address: Address;
  numberOfBedrooms: number;
  numberOfBathrooms: number;
  incomeGenerated: number;

  constructor(properties?: any) {
    this.id = properties && properties.id || 0;
    this.airbnbId = properties && properties.airbnbId || 0;
    this.owner = properties && properties.owner || "owner";
    this.address = properties && properties.address;
    this.numberOfBedrooms = properties && properties.numberOfBedrooms || 0;
    this.numberOfBathrooms = properties && properties.numberOfBathrooms || 0;
    this.incomeGenerated = properties && properties.incomeGenerated || 0;
  }
}
