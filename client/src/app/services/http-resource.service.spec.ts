import { TestBed, inject } from '@angular/core/testing';

import { HttpResourceService } from './http-resource.service';

describe('HttpResourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpResourceService]
    });
  });

  it('should be created', inject([HttpResourceService], (service: HttpResourceService) => {
    expect(service).toBeTruthy();
  }));
});
