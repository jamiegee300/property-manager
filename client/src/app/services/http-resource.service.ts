import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class HttpResourceService {
  private options;
  readonly csrfToken: HTMLInputElement;

  constructor(private http: HttpClient) {
    //this.csrfToken = <HTMLInputElement>document.getElementById('csrf');

    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        //'x-csrf-token': this.csrfToken.value
      }),
      responseType: 'json',
      observe: 'body'
    };
  }

  static appendTimestamp(url: string) {
    return url +
          ((url.indexOf('?') === -1) ? "?" : "&") +
          "now=" + new Date().valueOf();
  }

  send(url: string, data: any, options = this.options) {
    return this.http.post(url, data, options);
  }

  retrieve(url: string, options = this.options) {
    if (!options.allowCache)
      url = HttpResourceService.appendTimestamp(url);

    return this.http.get<object>(url, options);
  }

}
