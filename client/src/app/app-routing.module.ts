import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertiesComponent } from './components/properties/properties.component';
import { PropertyComponent } from './components/property/property.component';
import { CreateComponent } from './components/create/create.component';

const routes: Routes = [
  { path: 'app/properties', component: PropertiesComponent },
  { path: 'app/property/:id', component: PropertyComponent },
  { path: 'app/create', component: CreateComponent },
  { path: 'app', redirectTo: 'app/properties', pathMatch: 'full' },
  { path: '', redirectTo: 'app/properties', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class AppRoutingModule { }
