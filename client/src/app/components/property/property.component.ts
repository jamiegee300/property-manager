import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { HttpResourceService } from "../../services/http-resource.service";
import { Property } from "../../model/Property";

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  propertyUrl: string = "/api/property";
  id: number;
  feedback: string;
  property: Property;

  constructor(private route: ActivatedRoute, private httpResourceService: HttpResourceService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      const uri = this.propertyUrl + '/' + this.id;

      this.httpResourceService.retrieve(uri)
                              .subscribe((result): void => {
                                  this.property = new Property(result['property']);
                              });
    });
  }

  update() {
      const uri = this.propertyUrl + '/' + this.id;

      this.httpResourceService.send(uri, this.property)
                              .subscribe((result): void => {
                                console.log("Updated");
                              });
  }

}
