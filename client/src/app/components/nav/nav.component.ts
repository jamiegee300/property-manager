import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  currentPath: string;

  constructor(private router: Router) {
    router.events.subscribe((event: Event) => {

      if (event instanceof NavigationStart) {
        console.log('Started change route');
      }

      if (event instanceof NavigationEnd) {
        console.log('Finished change route', event);
        this.currentPath = event.url;
      }

      if (event instanceof NavigationError) {
        console.log('Error changing route', event.error);
      }

    });
  }

  ngOnInit() {
  }

  setActiveClass(path) {
    return { 'active': path === this.currentPath };
  }

  goTo(path) {
    this.router.navigateByUrl(path);
  }

}
