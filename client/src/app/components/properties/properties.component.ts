import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { HttpResourceService } from "../../services/http-resource.service";
import { Property } from "../../model/Property";
import { Option } from "../../model/Option";

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {
  propertiesUrl: string = "/api/properties";
  selectedBedrooms: string = "any";
  selectedBathrooms: string = "any";
  bedroomOptions: Array<Option> = [
    { text: "Any", value: "any" },
    { text: "2", value: "2" },
    { text: "3", value: "3" },
    { text: "4", value: "4" },
    { text: "5+", value: "5+" },
  ];
  bathroomOptions: Array<Option> = [
    { text: "Any", value: "any" },
    { text: "2", value: "2" },
    { text: "3", value: "3" },
    { text: "4", value: "4" },
    { text: "5+", value: "5+" },
  ];
  properties: Array<Property>;

  constructor(private router: Router, private httpResourceService: HttpResourceService) { }

  ngOnInit() {
    this.search();
  }

  search(): void {
      const uri = this.propertiesUrl +
                  "?bedrooms=" + this.selectedBedrooms +
                  "&bathrooms=" + this.selectedBathrooms;

      this.httpResourceService.retrieve(uri)
                              .subscribe((results): void => {
                                console.log("RESULTS: ", results);

                                  this.properties = results['properties'].map((value) => {
                                      return new Property(value);
                                  });
                              });
  }

  goToProperty(id) {
    this.router.navigateByUrl('/app/property/' + id);
  }

}
