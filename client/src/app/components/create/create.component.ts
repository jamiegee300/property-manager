import { Component, OnInit } from '@angular/core';
import { HttpResourceService } from "../../services/http-resource.service";
import { Property } from "../../model/Property";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  createUrl: string = "/api/property/create";
  airBnbIdUrl: string = "/api/airbandbid";
  property: Property;
  feedback: string;

  constructor(private httpResourceService: HttpResourceService) { }

  ngOnInit() {
    this.property = new Property();
  }

  sendProperty() {
    this.httpResourceService.send(this.createUrl, this.property)
                            .subscribe((result): void => {

                            });
  }

  create() {
    const url = this.airBnbIdUrl + '/' + this.property.airbnbId;

    this.httpResourceService.retrieve(url, this.property)
                            .subscribe((result): void => {
                              console.log("RESULT: ", result);
                                //if (result.status === 200)
                                  //this.sendProperty();

                                this.feedback = "Invalid Air BnB Id";
                            });
  }


}
