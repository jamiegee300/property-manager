module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        mochaTest: {
            test: {
                src: ['tests/specs/**/*.js']
            }
        },

        server: {
            options: {
                port: 3000
            }
        },

        nodemon: {
            development: {
                options: {
                    file: '/server.js',
                    args: [''],
                    watchedExtensions: ['js'],
                    watchedFolders: ['lib', 'routes', 'channels']
                }
            }
        },

        open: {
            server: {
                path: 'http://localhost:<%= server.options.port %>'
            }
        },

        concurrent: {
            server: {
                tasks: ['nodemon:development', 'open:server'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        uglify: {
            my_target: {
                files: {
                    'public/app/main.js': [ 'public/app/main.min.js' ]
                }
            }
        },

        concat: {
            dependencies: {
                src: [
                    'public/libs/jquery/dist/jquery.min.js',
                    'public/libs/jquery/bootstrap-4.1.3/dist/js/bootstrap.min.js'
                ],
                dest: 'public/js/dependencies.js'
            }
        }

    });

    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('server', [ 'concurrent:server' ]);
    grunt.registerTask('test', ['mochaTest']);
    grunt.registerTask('build', [
        'test',
        'concat'
    ]);

    grunt.registerTask('default', ['test']);

};