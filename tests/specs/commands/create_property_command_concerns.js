const expect = require('expect.js');
const sinon = require('sinon');

const Command = require('../../../lib/commands/Commands');
const address = {
    id: 45,
    line1: "Line 1",
    line3: "Line 3",
    line4: "Line 4",
    postCode: "123",
    city: "London",
    country: "UK"
};
const stubs = {
    database: {
        ensureSchema: sinon.stub(),
        query: sinon.stub().yields(null, { rows: [ {
                id: 45,
                line1: "Line 1",
                line3: "Line 3",
                line4: "Line 4",
                postCode: "123",
                city: "London",
                country: "UK"
            } ] })
    },
    settings: {
        database: {
            query: sinon.stub().yields(null)
        }
    },
    Commands: Command
};
const command = new Command(stubs);

describe('Given I have details of a property', function () {

    const id = 12;
    const values = {
        owner: "James",
        airbnbId: 2020202,
        address: address,
        numberOfBedrooms: 2,
        numberOfBathrooms: 1,
        incomeGenerated: 200.5
    };

    describe('When calling a command to create the property by id', function () {

        it('Should execute a database query to create the address', function (done) {

            command.createProperty({ id, values }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('INSERT INTO address ( line1, line3, line4, postCode, city, country ) VALUES ( "Line 1", "Line 3", "Line 4", "123", "London", "UK" )')).to.eql(true);
                done();
            });

        });

        it('Should execute a database query to create the property', function (done) {

            command.createProperty({ id, values }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('INSERT INTO property (owner, airbnbId, address, numberOfBedrooms, numberOfBathrooms, incomeGenerated) VALUES ("James", "2020202", "45", "2", "1", "200.5")')).to.eql(true);
                done();
            });

        });

    });

});