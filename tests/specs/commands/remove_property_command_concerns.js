const expect = require('expect.js');
const sinon = require('sinon');

const Command = require('../../../lib/commands/Commands');
const addressId = 7;
const stubs = {
    database: {
        ensureSchema: sinon.stub(),
        query: sinon.stub().yields(null, { rows: [ { address: addressId } ] })
    },
    settings: {
        database: {
            query: sinon.stub().yields(null)
        }
    },
    Commands: Command
};
const command = new Command(stubs);

describe('Given I have a property', function () {

    const id = 12;

    describe('When calling a command to remove the property by id', function () {

        it('Should execute a database query to remove the address', function (done) {

            command.removeProperty({ id }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith("DELETE FROM address WHERE id = 7")).to.eql(true);
                done();
            });

        });

        it('Should execute a database query to remove the property', function (done) {

            command.removeProperty({ id }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith("DELETE FROM property WHERE id = 12")).to.eql(true);
                done();
            });

        });

    });

});