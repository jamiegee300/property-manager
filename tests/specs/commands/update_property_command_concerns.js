const expect = require('expect.js');
const sinon = require('sinon');

const Command = require('../../../lib/commands/Commands');
const stubs = {
    database: {
        ensureSchema: sinon.stub(),
        query: sinon.stub().yields(null)
    },
    settings: {
        database: {
            query: sinon.stub().yields(null)
        }
    },
    Commands: Command
};
const command = new Command(stubs);

describe('Given I have a property', function () {

    describe('When calling a command to update the property by id', function () {

        it('Should execute a database query to update the address', function (done) {

            const id = 12;
            const updates = {
                owner: "Jamie",
                numberOfBathrooms: 2,
                address: {
                    id: 17,
                    line1: "line 1",
                    line3: "Line 3",
                    line4: "line 4",
                    postCode: "PL286FG",
                    city: "Plymouth",
                    country: "UK"
                }
            };

            command.updateProperty({ id, updates }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('UPDATE address SET id = "17", line1 = "line 1", line3 = "Line 3", line4 = "line 4", postCode = "PL286FG", city = "Plymouth", country = "UK" WHERE id = 17')).to.eql(true);
                done();
            });

        });

        it('Should execute a database query to update the property', function (done) {

            const id = 12;
            const updates = {
                owner: "Jamie",
                numberOfBathrooms: 2,
                address: {
                    id: 17,
                    line1: "line 1",
                    line3: "Line 3",
                    line4: "line 4",
                    postCode: "PL286FG",
                    city: "Plymouth",
                    country: "UK"
                }
            };

            command.updateProperty({ id, updates }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('UPDATE property SET owner = "Jamie", numberOfBathrooms = "2" WHERE id = 12')).to.eql(true);
                done();
            });

        });

    });

});