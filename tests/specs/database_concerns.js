const expect = require('expect.js');
const sinon = require('sinon');

const Database = require('../../lib/Database');
const settings = {
    database: {
        schema: [
            'CREATE TABLE IF NOT EXISTS property (id integer, airbnbId integer, owner varchar, address varchar, numberOfBedrooms integer, numberOfBathrooms integer, incomeGenerated double)',
            'CREATE TABLE IF NOT EXISTS another (id integer, name varchar)'
        ]
    }
};
const result = { test: true };
const client = {
    connect: sinon.stub().yields(null),
    query: sinon.stub().yields(null, result)
};

describe('Given I have a database client', function () {

    describe('when instantiating', function () {

        const database = new Database(settings, client);

        it('should connect', function (done) {
            expect(client.connect.called).to.eql(true);
            done();
        });

        it('should call a query to ensure the schema is correct', function (done) {
            expect(client.query.calledWith(settings.database.schema[0], [])).to.eql(true);
            expect(client.query.calledWith(settings.database.schema[1], [])).to.eql(true);
            done();
        });

    });

});

describe('Given I have a database client', function () {

    const database = new Database(settings, client);

    describe('when calling query passing in query to execute in the form of a string', function () {

        const queryToExecute = 'SELECT * FROM property WHERE id = 12 LIMIT 1';

        it('should execute the query', function (done) {
            database.query(queryToExecute, function (err) {
                if (err) console.log("Error - ", err);

                expect(client.query.calledWith(queryToExecute, [])).to.eql(true);
                done();
            });
        });

        it('should return the result of the query', function (done) {
            database.query(queryToExecute, function (err, res) {
                if (err) console.log("Error - ", err);

                expect(res).to.eql(result);
                done();
            });
        });

    });

});