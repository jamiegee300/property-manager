const expect = require('expect.js');
const sinon = require('sinon');

const result = {
    rows: [ { name: "Property 12" } ]
};
const stubs = {
    database: {
        ensureSchema: sinon.stub(),
        query: sinon.stub().yields(null, result)
    },
    settings: {
        database: {
            query: sinon.stub().yields(null)
        }
    }
};
const Queries = require('../../../lib/queries/Queries');
const queries = new Queries(stubs);

describe('Given I have a property', function () {

    const id = 12;

    describe('When calling a query to get the property by id', function () {

        it('Should execute a database query to get the property', function (done) {

            queries.getPropertyById({ id }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith("SELECT * FROM property LEFT JOIN address ON property.address = address.id WHERE property.id = 12 LIMIT 1")).to.eql(true);
                done();
            });

        });

        it('Should return the data returned by the database', function (done) {

            queries.getPropertyById({ id }, function (err, res) {
                if (err) console.log("Error - ", err);

                expect(res).to.eql(result.rows[0]);
                done();
            });

        });

    });

});