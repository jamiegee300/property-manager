const expect = require('expect.js');
const sinon = require('sinon');

const results = {
    rows: [ { name: "Property 12" }, { name: "Property 14"} ]
};
const stubs = {
    database: {
        ensureSchema: sinon.stub(),
        query: sinon.stub().yields(null, results)
    },
    settings: {
        database: {
            query: sinon.stub().yields(null)
        }
    }
};
const Queries = require('../../../lib/queries/Queries');
const queries = new Queries(stubs);

describe('Given I have a property', function () {

    describe('When calling a query to get properties by bedrooms and bathrooms', function () {

        const bedrooms = "5+";
        const bathrooms = "3";

        it('Should execute a database query to get the properties', function (done) {

            queries.getPropertiesBySearchTerm({ bedrooms, bathrooms }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('SELECT * FROM property LEFT JOIN address ON property.address = address.id WHERE property.numberOfBedrooms >= 5 AND property.numberOfBathrooms = 3')).to.eql(true);
                done();
            });

        });

        it('Should return the data returned by the database', function (done) {

            queries.getPropertiesBySearchTerm({ bedrooms, bathrooms }, function (err, res) {
                if (err) console.log("Error - ", err);

                expect(res).to.eql(results.rows);
                done();
            });

        });

    });

    describe('When calling a query to get properties by just bedrooms', function () {

        const bedrooms = "4";
        const bathrooms = "any";

        it('Should execute a database query to get the properties', function (done) {

            queries.getPropertiesBySearchTerm({ bedrooms, bathrooms }, function (err) {
                if (err) console.log("Error - ", err);

                expect(stubs.database.query.calledWith('SELECT * FROM property LEFT JOIN address ON property.address = address.id WHERE property.numberOfBedrooms = 4')).to.eql(true);
                done();
            });

        });

    });

});