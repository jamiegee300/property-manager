const expect = require('expect.js');
const sinon = require('sinon');
const Model = require('../../../lib/Model');

const PropertyService = require('../../../lib/services/PropertyService');

const property1 = new Model.Property({ id: 12, airbnbId: 1334159, owner: "Kev", address: { line1: "Number 17", line4: "Main rd", postCode: "LDN281", city: "London", country: "UK" }, numberOfBedrooms: 3, numberOfBathrooms: 2, incomeGenerated: 1200.5 });
const property2 = new Model.Property({ id: 13, airbnbId: 1334157, owner: "John", address: {line1: "Number 11", line4: "Main rd", postCode: "LDN282", city: "Swindon", country: "UK" }, numberOfBedrooms: 2, numberOfBathrooms: 2, incomeGenerated: 1100.6 });

const stubs = {
    Model: Model,
    queries: {
        getPropertiesBySearchTerm: sinon.stub().yields(null, [ property1, property2 ]),
        getPropertyById: sinon.stub().yields(null, property1)
    },
    commands: {
        createProperty: sinon.stub().yields(null),
        updateProperty: sinon.stub().yields(null),
        removeProperty: sinon.stub().yields(null)
    },
    eventTracker: {
        track: sinon.stub().yields(null)
    }
};

describe('Given I have properties and a property service', function () {

    const propertyService = new PropertyService(stubs);

    describe('When I call the property service search method passing in a number of bedrooms and bathrooms', function () {

        it('Should call a query to get properties by search term passing in the search term', function (done) {
            const bedrooms = "5+";
            const bathrooms = "3";

            propertyService.search({ bedrooms, bathrooms })
                           .then(() => {
                               expect(stubs.queries.getPropertiesBySearchTerm.calledWith({ bedrooms, bathrooms })).to.eql(true);
                               done();
                           })
                           .catch(err => {
                               console.log("ERROR - ", err);
                           });
        });

        it('Should return the matching properties from the query', function (done) {
            const searchTerm = "proper";

            propertyService.search({ searchTerm })
                           .then(results => {
                               expect(results).to.eql([ property1, property2 ]);
                               done();
                           })
                           .catch(err => {
                               console.log("ERROR - ", err);
                           });
        });

    });

    describe('When I call the property service get method passing in an id', function () {

        it('Should call a query to get property by id passing in the id', function (done) {
            const id = 12;

            propertyService.get({ id })
                           .then(() => {
                               expect(stubs.queries.getPropertyById.calledWith({ id })).to.eql(true);
                               done();
                           })
                           .catch(err => {
                               console.log("ERROR - ", err);
                           });
        });

        it('Should return the matching property from the query', function (done) {
            const id = 12;

            propertyService.get({ id })
                           .then(result => {
                               expect(result).to.eql(property1);
                               done();
                           })
                           .catch(err => {
                               console.log("ERROR - ", err);
                           });
        });

    });

});