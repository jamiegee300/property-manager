const expect = require('expect.js');
const Model = require('../../../lib/Model');

describe('Given I have details of an event', function () {

    const name = "thing:created";
    const payload = {
        "some": "details",
        "included": "here",
        "happened": new Date("2010-01-01")
    };
    const stringifiedPayload = encodeURIComponent("{\"some\":\"details\",\"included\":\"here\",\"happened\":\"2010-01-01T00:00:00.000Z\"}");

    describe('when I create an event', function () {

        const event = new Model.Event({ name, payload });

        it('should have the correct name', function () {
            expect(event.name).to.eql(name);
        });

        it('should have the correct payload serialised into a string', function () {
            expect(event.payload).to.eql(stringifiedPayload);
        });

    });

});

describe('Given I have an event', function () {

    const name = "thing:created";
    const payload = {
        "some": "details",
        "included": "here",
        "happened": new Date("2010-01-01")
    };

    const event = new Model.Event({ name, payload });

    describe('when calling getPayload', function () {

        const returnedPayload = event.getPayload();

        it('should return the payload parsed back into JSON', function () {
            expect(returnedPayload).to.eql(payload);
        });

    });

});