const expect = require('expect.js');
const Model = require('../../../lib/Model');

describe('Given I have details of a property', function () {

    const id = 12;
    const airbnbId = 1334159;
    const owner = "Kev";
    const address = {
        line1: "Number 17",
        line2: "House name",
        line4: "Main rd",
        postCode: "LDN281",
        city: "London",
        country: "UK"
    };
    const numberOfBedrooms = 3;
    const numberOfBathrooms = 2;
    const incomeGenerated = 1200.5;

    describe('when I create a property', function () {

        const property = new Model.Property({
            id,
            airbnbId,
            owner,
            address,
            numberOfBedrooms,
            numberOfBathrooms,
            incomeGenerated
        });

        it('should have the correct id', function () {
            expect(property.id).to.eql(id);
        });

        it('should have the correct airbnbId', function () {
            expect(property.airbnbId).to.eql(airbnbId);
        });

        it('should have the correct owner', function () {
            expect(property.owner).to.eql(owner);
        });

        it('should have the correct address', function () {
            expect(property.address instanceof Model.Address).to.eql(true);
            expect(property.address.line1).to.eql(address.line1);
            expect(property.address.line2).to.eql(address.line2);
            expect(property.address.line3).to.eql(address.line3);
            expect(property.address.line4).to.eql(address.line4);
            expect(property.address.postCode).to.eql(address.postCode);
            expect(property.address.city).to.eql(address.city);
            expect(property.address.country).to.eql(address.country);
        });

        it('should have the correct numberOfBedrooms', function () {
            expect(property.numberOfBedrooms).to.eql(numberOfBedrooms);
        });

        it('should have the correct numberOfBathrooms', function () {
            expect(property.numberOfBathrooms).to.eql(numberOfBathrooms);
        });

        it('should have the correct incomeGenerated', function () {
            expect(property.incomeGenerated).to.eql(incomeGenerated);
        });

    });

});