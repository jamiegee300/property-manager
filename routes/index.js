const debug = require('debug')('app:routes:index');
const express = require('express');
const router = express.Router();

module.exports = function (dependencies) {

    router.get('/*', function(req, res, next) {
        res.render('index', { title: 'Property Manager' });
    });

    return router;

};
