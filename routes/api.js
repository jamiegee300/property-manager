const debug = require('debug')('app:routes:api');
const express = require('express');
const router = express.Router();

module.exports = function (dependencies) {
    const propertyService = dependencies.propertyService;
    const airBAndBService = dependencies.airBAndBService;

    router.get('/properties/?', function(req, res, next) {
        const bedrooms = req.query.bedrooms;
        const bathrooms = req.query.bathrooms;

        propertyService.search({ bedrooms, bathrooms })
                       .then(properties => {
                           res.status(200).send({ properties });
                       })
                       .catch(err => {
                           debug('Error searching for properties ', err);
                           res.sendStatus(500);
                       });
    });

    router.get('/property/:id/?', function(req, res, next) {
        const id = req.params.id;

        propertyService.get({ id })
                       .then(property => {
                           res.status(200).send({ property });
                       })
                       .catch(err => {
                           debug('Error retrieving property ', err);
                           res.sendStatus(500);
                       });
    });

    router.post('/property/?', function(req, res, next) {
        const values = req.body;

        propertyService.create({ values })
                       .then(() => {
                           res.sendStatus(202);
                       })
                       .catch(err => {
                           debug('Error creating property ', err);
                           res.sendStatus(500);
                       });
    });

    router.post('/property/:id/?', function(req, res, next) {
        const id = req.params.id;
        const updates = req.body;

        propertyService.update({ id, updates })
                       .then(() => {
                           res.sendStatus(202);
                       })
                       .catch(err => {
                           debug('Error updating property ', err);
                           res.sendStatus(500);
                       });
    });

    router.delete('/property/:id/?', function(req, res, next) {
        const id = req.params.id;

        propertyService.remove({ id })
                       .then(() => {
                           res.sendStatus(202);
                       })
                       .catch(err => {
                           debug('Error removing property ', err);
                           res.sendStatus(500);
                       });
    });

    router.get('/airbandbid/id/?', function (req, res, next) {
        airBAndBService.validateId(req.params.id)
                       .then(() => {
                           res.sendStatus(200);
                       })
                       .catch(err => {
                           res.sendStatus(404);
                       });
    });

    return router;

};
